# Laboratorio_1_Pizzas

**Laboratorio 1**

Requisitos de finalización
Apertura: jueves, 2 de febrero de 2023, 19:00
Cierre: jueves, 9 de febrero de 2023, 17:00
Información sobre la tarea.

**Instrucciones.**

El documento adjunto contiene un enunciado el cual debe desarrollar en el lenguaje Java.
Debe crear un único proyecto que contenga el ejercicio, puede crear las clases que considere necesarias.
Debe utilizar interfaz gráfica, archivos, objetos, estructura de capas, estructuras de datos, entre otros. 
Todo el proyecto debe estar en un repositorio de GitLab.
Entrega.

Debe subir por este mismo medio el proyecto en un zip, además, dentro del mismo zip debe incluir un documento de texto con el URL de GitLab donde ubicó el proyecto.
Además, incluya en el zip los archivos que tenga que crear en el proyecto.
La fecha y hora límite de entrega es el jueves 09 de febrero a las 05:00 PM.
Evaluación.

La entrega esta relacionada al porcentaje de Laboratorios correspondiente al curso.
El docente revisa cada laboratorio individualmente y asigna la nota en base a la rúbrica.
